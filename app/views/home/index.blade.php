@extends('layouts.master')

@section('content')
  	<div class="jumbotron hidden-xs">
      <div class="container ">
        <h1>Bem-Vindo</h1>
        <p>
          Criei esse sistema para facilitar a vida de quem tem o cartão da Personal, pois eles tem um sistema não muito acessível para buscar os lugares onde há a possibilidade de pagar com o vale alimentação/refeição.
          Toda informação foi retirada inteiramente do site deles, e é atualizada frenquentemente, não me responsabilizo por lugares que estão no sistema e não aceitam mais o cartão.<br>
          Sugiro entrar em contato com o local por telefone antes para ter certeza.<br>
          Bom almoço, jantar ou lanche para você :)
        </p>
      </div>
    </div>

    <div class="container">
      @include('partials.form-city-estate')
    </div>
    <br>
@stop