<div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ URL::to('/') }}">Personal - Busca</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li {{ Request::is('/') ? 'class="active"' : '' }}  ><a href="{{ URL::to('/') }}">Home</a></li>
        <li {{ Request::is('place') || Request::is('place/*') ? 'class="active"' : '' }}><a href="{{ URL::to('/place') }}">Lugares</a></li>
        <li {{ Request::is('map') ? 'class="active"' : '' }}><a href="{{ URL::to('/map') }}" title="Ver o que está próximo a mim">Proximidade</a></li>
        <!-- <li {{ Request::is('about') ? 'class="active"' : '' }}><a href="{{ URL::to('/about') }}">Sobre</a></li> -->
        <li {{ Request::is('contact') ? 'class="active"' : '' }}><a href="{{ URL::to('/contact') }}">Contato</a></li>
      </ul>
    </div><!-- /.nav-collapse -->
  </div><!-- /.container -->
</div><!-- /.navbar -->