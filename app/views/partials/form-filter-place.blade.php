{{ Form::open(array('url' => 'place/search', 'method' => 'get', 'class' => 'form-inline', 'role' => 'form')) }}

  <div class="form-group">
    {{ Form::label('name', 'Nome', array('class' => 'sr-only')) }}
    {{ Form::text('name', isset($search['name']) ? $search['name'] : null , array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) }}
  </div>
  &nbsp;
  <div class="form-group">
    {{ Form::label('district', 'Bairro', array('class' => 'sr-only')) }}
    {{ Form::text('district', isset($search['district']) ? $search['district'] : null, array('placeholder' => 'Filtrar por bairro', 'class' => 'form-control')) }}
  </div>
  &nbsp;
  <div class="form-group">
    {{ Form::label('adress', 'Endereço', array('class' => 'sr-only')) }}
    {{ Form::text('adress', isset($search['adress']) ? $search['adress'] : null, array('placeholder' => 'Filtrar por endereço', 'class' => 'form-control')) }}
  </div>
  &nbsp;
  <div class="form-group">
    {{ Form::label('city', 'Cidade', array('class' => 'sr-only')) }}
    {{ Form::select('city', array('BLUMENAU' => 'BLUMENAU','BRUSQUE' => 'BRUSQUE', 'FLORIANÓPOLIS' => 'FLORIANÓPOLIS'), isset($search['city']) ? $search['city'] : null, array('class' => 'form-control')) }}
  </div>
  &nbsp;
  <div class="form-group">
    {{ Form::label('state', 'Estado', array('class' => 'sr-only')) }}
    {{ Form::select('state', array('SC' => 'SC'), isset($search['state']) ? $search['state'] : null, array('class' => 'form-control')) }}
  </div>
  &nbsp;
  {{ Form::submit('Buscar', array('class' => 'btn btn-primary')) }}
  {{ Form::token() }}

{{ Form::close() }}   