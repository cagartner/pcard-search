{{ Form::open(array('url' => 'place/search', 'method' => 'get', 'class' => 'form-horizontal', 'role' => 'form')) }}
  <legend>Selecione seu Estado e Cidade</legend>
  <div class="form-group">
    {{ Form::label('state', 'Estado', array('class' => 'col-sm-1 control-label')) }}
    <div class="col-sm-11">
      {{ Form::select('state', array('SC' => 'SC'), isset($search['state']) ? $search['state'] : null, array('class' => 'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('city', 'Cidade', array('class' => 'col-sm-1 control-label')) }}
    <div class="col-sm-11">
      {{ Form::select('city', array('BLUMENAU' => 'BLUMENAU','BRUSQUE' => 'BRUSQUE', 'FLORIANÓPOLIS' => 'FLORIANÓPOLIS'), isset($search['city']) ? $search['city'] : null, array('class' => 'form-control')) }}
    </div>
  </div>       

  {{ Form::submit('Buscar', array('class' => 'btn btn-primary btn-lg btn-block')) }}
  {{ Form::token() }}

{{ Form::close() }}