@extends('layouts.mapa')

{{-- Cabeçalho --}}
@section('header')
  <div class="alert alert-info alert-dismissable alert-map affix" data-spy="affix" data-offset-top="55" style="position: absolute">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Olá!</strong> Para ver os locais no mapa, você precisa autorizar a página a pegar sua localização atual.
  </div>
  @parent  
@stop

{{-- Conteúdo na página --}}
@section('content')   
	
   <div id="nearest"></div>

@stop
{{-- /End Conteúdo --}}

{{-- EXIBE javascript usados nessa página --}}
@section('scripts')
  @parent  
  <script>
      $('.affix').affix();

      $('#nearest').css({
        height: $(window).height() - 142 + 'px'
      });

     // Gera mapa
      var map = new GMaps({
          div: '#nearest',
          lat: 0,
          lng: 0
      });

      // Busca sua coordenada
      GMaps.geolocate({
        success: function(position) {

          $('.alert-map').fadeOut();

          var linkShow = '{{URL::to('place/show')}}/';

          // Busca lugares perto
          $.ajax({
            dataType: "json",            
            url: '{{ URL::to('map/nearest') }}',

            data: {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            },
            success: function ( data ) {

              $.each( data, function ( index, place ) {

                  var distance = place.distance.toString();
                  distance = distance.slice(0,5);

                  map.addMarker({
                    lat: place.lat,
                    lng: place.log,
                    title: place.name,
                    click: function(e) {
                    },
                    infoWindow: {
                      content: '<h4>'+place.name+'</h4>'+
                               '<p>Telefone: <strong>'+place.phone+'</strong></p>'+ 
                               '<p>'+place.adress+','+place.adress_number+' - '+place.district+','+place.city+'/'+place.state+'</p>'+
                               '<p><span class="muted">Distancia Apróx.: '+distance+' Km</span> <a class="btn btn-primary pull-right" href="'+linkShow+place.id+'">Ver Lugar</a> </p>'
                    }
                  });
              });

            }
          });

          map.addMarker({
            lat: position.coords.latitude,
            lng: position.coords.longitude,
            title: 'Você está por aqui!',
            click: function(e) {
            },
            icon : {
              size : new google.maps.Size(48, 48),
              url : 'http://icons.iconarchive.com/icons/custom-icon-design/flatastic-3/48/user-icon.png'
            },
            infoWindow: {
              content: '<h4>Olá você está perto daqui!</h4>'+
                       '<p>Esse carinha aqui mostra que você está perto dessa posição :)</p>',
              zIndex: 999999999999
            }
          });

          map.setCenter(position.coords.latitude, position.coords.longitude);
        },
        error: function(error) {

        },
        not_supported: function() {

        },
        always: function() {
        
        }
      });

  </script>
@stop