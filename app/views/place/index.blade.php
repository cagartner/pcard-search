@extends('layouts.master')

@section('header')
  @parent  
  <div class="container">
    <div class="well">
      @include('partials.form-filter-place')
    </div>
  </div>
@stop

@section('content')   
    
  <p>Exibindo <strong>{{ count( $places ) }}</strong> de <strong>{{ $places->getTotal() }}</strong> lugares encontrados</p>

  <table class="table table-striped">     
    <thead>
      <tr>
        <th>Nome</th>
        <th>Telefone</th>
        <th>Endereço</th>
        <th>Bairro</th>
        <th>Mapa</th>
      </tr>
    </thead>

    <tbody>
      @if (count($places) > 0)

        @foreach ($places as $place)
        <tr>
            <td>{{ $place->name }}</td>
            <td>{{ $place->phone }}</td>
            <td>{{ $place->adress }}, {{ $place->adress_number }}</td>
            <td>{{ $place->district }}</td>
            <td><a href="{{ URL::to('place/show/' . $place->id) }}" class="btn btn-success">Visualizar</a></td>
        </tr>
        @endforeach
        
      @else
        <tr>
          <td colspan="5" class="warning">Não há registro :(</td>
        </tr>
      @endif
      
    </tbody>

  </table>

  {{ $places->links() }}
@stop