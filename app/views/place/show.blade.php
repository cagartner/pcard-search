@extends('layouts.master')

{{-- Cabeçalho --}}
@section('header')
  @parent  
  <div class="container">
  	<a href="{{ URL::previous() }}"><< Voltar</a>
    <h2>{{ $place->name }}</h2>
  </div>
@stop

{{-- Conteúdo na página --}}
@section('content')   
	
    <table class="table table-striped">     
     
      <tbody>
        <tr>
          <td class="col-sm-1">Telefone: </td>
          <td><strong>{{ $place->phone }}</strong></td>
        </tr>
      	<tr>
      		<td class="col-sm-1">Endereço: </td>
      		<td>{{ $place->adress }}, {{ $place->adress_number }}</td>
      	</tr>
      	<tr>
      		<td class="col-sm-1">Bairro: </td>
      		<td>{{ $place->district }}</td>
      	</tr>
      	<tr>
      		<td class="col-sm-1">Cidade/UF: </td>
      		<td>{{ $place->city }} / {{ $place->state }}</td>
      	</tr>
      </tbody>

  </table>
  
  <div class="row">

    <div class="col-sm-6">
      <h3>Mapa:</h3>
      <div id="mapaaa"></div>
    </div>

    <div class="col-sm-6">
      <h3>Visualização da Rua:</h3>
      <div id="panorama"></div>
    </div>
  </div>  

@stop
{{-- /End Conteúdo --}}

{{-- EXIBE javascript usados nessa página --}}
@section('scripts')
  @parent  
  <script>
     // Gera mapa
      var map = new GMaps({
          div: '#mapaaa',
          lat: {{ $place->lat }},
          lng: {{ $place->log }}
      });

      // Gera mapa
      var panorama = GMaps.createPanorama({
        el: '#panorama',
        lat: {{ $place->lat }},
        lng: {{ $place->log }}
      });

      // Adiciona marca para o cliente
      map.addMarker({
        lat: {{ $place->lat }},
        lng: {{ $place->log }},
        title: '{{ $place->name }}',
        click: function(e) {
        },
        infoWindow: {
          content: '<h4>{{ $place->name }}</h4><br><p>{{ $place->adress }}, {{ $place->adress_number }} - {{ $place->district }}, {{ $place->city }} / {{ $place->state }}</p>'
        }
      });
  </script>
@stop