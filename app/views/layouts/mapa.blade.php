<!DOCTYPE html>
<html>
  <head>
    <title>Personal Busca - Carlos Gartner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
    <!-- SEO -->
    <meta name="description" content="Sistema criado para facilitar a busca de lugares que aceitam Personal Card para alimentação/refeição.">
    <meta name="keywords" content="personal card blumenau, personal, lanchonetes personal blumenau, lanchonete, padaria, personal alimentação">
    <meta name="robots" content="index, follow">

    <!-- Bootstrap -->
    <HTML>
    {{ HTML::style("dist/css/bootstrap.min.css") }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
		body {
			padding-top: 50px;
		}

    #nearest {
        display: block;
        width: 100%;
        height: 500px;
    }

    #nearest {
        background: #58B;
    }

    footer {
      height: 20px;
      padding-bottom: 10px;
      margin-top: 30px;
    }
    </style>
  </head>
  <body>
  @include('includes.header')
	
  	@section('header')    
    @show

    @yield('content')

    <footer>
      <div class="container text-right">
        Desenvolvido por <a href="https://www.facebook.com/CarlosGartner" target="_blank">Carlos Augusto Gartner</a>
      </div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    {{ HTML::script("https://code.jquery.com/jquery.min.js") }}    
    {{ HTML::script("http://maps.google.com/maps/api/js?sensor=true", array('type' => 'text/javascript')) }}
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {{ HTML::script("dist/js/bootstrap.min.js") }}    
    {{ HTML::script("js/gmaps.js") }}   
    @section('scripts')    
    @show

     <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-46320222-1', 'carlosgartner.com.br');
      ga('send', 'pageview');

     </script>
  </body>
</html>