@extends('layouts.master')

@section('content')

    <div class="container">    

      @if (count($errors->all()) > 0)
      <div class="alert alert-danger alert-block" id="error" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>Opppss</h4>
        Por favor verifique nos campos os seguintes erros: <br>

        @foreach ($errors->all() as $erro)
            <li>@lang( $erro )</li>
        @endforeach
    
      </div>
      @endif

      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>Sucesso</h4>
          @if(is_array($message))
              @foreach ($message as $m)
                  {{ $m }}
              @endforeach
          @else
              {{ $message }}
          @endif
      </div>
      @endif

      @if ($message = Session::get('error'))
      <div class="alert alert-danger alert-block" role="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>Oppsss!</h4>
          @if(is_array($message))
          @foreach ($message as $m)
          {{ $m }}
          @endforeach
          @else
          {{ $message }}
          @endif
      </div>
      @endif
            
      {{ Form::open(array('url' => 'contact/send', 'method' => 'post', 'class' => 'form', 'role' => 'form')) }}

        <div class="form-group">
          {{ Form::label('name', 'Seu nome') }}
          {{ Form::text('name', '', array('placeholder' => 'Preencha seu nome', 'class' => 'form-control')) }}
        </div>

        <div class="form-group">
          {{ Form::label('email', 'Seu e-mail') }}
          {{ Form::text('email', '', array('placeholder' => 'Informe seu endereço de e-mail', 'class' => 'form-control')) }}
        </div>

        <div class="form-group">
          {{ Form::label('message', 'Sua mensagem') }}
          {{ Form::textarea('message', '', array('placeholder' => 'Me diga o que você tem em mente...', 'class' => 'form-control')) }}
        </div>
        
        {{ Form::submit('Enviar', array('class' => 'btn btn-primary')) }}
        {{ Form::token() }}

      {{ Form::close() }}   

    </div>
    <br>
@stop