@extends('layouts.master')

@section('content')
	{{ Form::open(array('url' => 'update/open', 'files' => true)) }}

		{{ Form::label('file', 'Arquivo CSV') }}
		{{ Form::file('file') }}
		{{ Form::select('state', array('SC' => 'SC')) }}
		{{ Form::select('city', array('BLUMENAU' => 'BLUMENAU', 'BRUSQUE' => 'BRUSQUE', 'FLORIANÓPOLIS' => 'FLORIANÓPOLIS')) }}
		{{ Form::submit('Atualizar Listagem') }}
		{{ Form::token() }}

	{{ Form::close() }}
@stop