<html>
<head>
  <title>Contato</title>
</head>
<body>
  <table width="600px">
    <tr>
      <td colspan="2">Novo contato realizado pelo Personal - Busca.</td>
    </tr>

    <tr>
      <td width="150">Nome</td>
      <td>{{ $contact->name }}</td>
    </tr>

    <tr>
      <td>E-mail</td>
      <td>{{ $contact->email }}</td>
    </tr>

    <tr>
      <td colspan="2">Mensagem:</td>
    </tr>    
    <tr>
      <td colspan="2">{{ nl2br($contact->message) }}</td>
    </tr>   

  </table>
</body>
</html>