<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

// Locais
Route::get('place', 'PlaceController@index');
Route::get('place/search', 'PlaceController@index');
Route::get('place/show/{id}', 'PlaceController@show');

// Rotas para atualização
Route::get('update', 'UpdateController@index');
Route::get('update/maps', 'UpdateController@maps');

// Envia arquivo
Route::post('update/open', 'UpdateController@open');

// Mapa
Route::get('map', 'MapController@index');
Route::get('map/nearest', 'MapController@nearest');

// Contato
Route::get('contact', 'ContactController@index');
Route::post('contact/send', 'ContactController@send');