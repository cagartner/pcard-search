<?php

use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function($table)
		{
		    $table->increments('id');
	        $table->string('email', 120);
	        $table->string('name', 120);
	        $table->longText('message');
	        $table->softDeletes();
	        $table->boolean('send');
	        $table->timestamps();
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacts');
	}

}