<?php

use Illuminate\Database\Migrations\Migration;

class CreatePlaceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('places', function($table)
    	{
	        $table->increments('id');	        
	        $table->string('name', 120);
	        $table->string('email', 120);
	        $table->string('phone', 15)->nullable()->default(null);
	        $table->string('adress', 200)->nullable()->default(null);
	        $table->string('adress_number', 200)->nullable()->default(null);
	        $table->string('complement', 120)->nullable()->default(null);
	        $table->string('district', 200)->nullable()->default(null);
	        $table->string('state', 200);
	        $table->string('city', 200);
	        $table->double('lat', 10,8)->nullable()->default(null);
	        $table->double('log', 10,8)->nullable()->default(null);
	        $table->enum('status', array('1', '0'))->default(1);
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('places');
	}

}