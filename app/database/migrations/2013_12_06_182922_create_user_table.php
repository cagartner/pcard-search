<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
    	{
	        $table->increments('id');
	        $table->string('email', 120)->unique();
	        $table->string('name', 120);
	        $table->string('password', 20);
	        $table->enum('status', array(1, 0))->default(1);
	        $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}