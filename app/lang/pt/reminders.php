<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Senha precisa ter no mínimo 6 caracteres e ser igual a sua confirmação.",

	"user"     => "Não conseguimos achar seu usuário pelo e-mail informado",

	"token"    => "O token informado é inválido",

);