<?php 
class Place extends Eloquent {

	protected $table = 'places';	
	protected $fillable = array('name', 'district', 'state', 'city');
}