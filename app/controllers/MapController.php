<?php
/**
 * Controller para exibir mapa
 * @author Carlos Augusto Gartner <contato@carlosgartner.com.br>
 */
class MapController extends \BaseController {

	/**
	 * Exibe página inicial da tela de locais próximos
	 * 
	 * @return View::make
	 */
	public function index()
	{
		return View::make('map.index');
	}

	/**
	 * Busca os locais próximos com os dados de latitude e longitude
	 * @return Response::json
	 */
	public function nearest() 
	{
		$lat = Request::get('lat');
		$lng = Request::get('lng');
		$distancia = 10; // Distancia em kilometros

		if ($lat && $lng) {
			$places = DB::select("SELECT * , ( 3956 * ACOS( COS( RADIANS( {$lat} ) ) * COS( RADIANS( lat ) ) * COS( RADIANS( log ) - RADIANS( {$lng} ) ) + SIN( RADIANS( {$lat} ) ) * SIN( RADIANS( lat ) ) ) ) AS distance FROM places HAVING distance < {$distancia} ORDER BY distance LIMIT 0 , 300");
			return Response::json( $places );	
		} else {
			$response = Response::make("Você precisa informar a latitude e a longitude", 203);
			$response->header('Content-Type', 'application/json');
			return $response;
		}
	}

}