<?php
/**
 * Controller de lugares
 * @author Carlos Augusto Gartner <github@com/cagartner>
 */
class PlaceController extends BaseController {

	/**
	 * Método inicial do controller, exibe locais com filtros por:
	 *
	 * - Nome 		[name]
	 * - Endereço 	[adress]
	 * - Bairro 	[district]
	 * - Cidade 	[city]
	 * - Estado 	[state]
	 *
	 * @return View::make
	 */
	public function index()
	{
		// Inicia query
		$query = Place::where('status', '=', 1);

		if (Input::has('name') ) // adiciona filtro por nome
		    $query->where('name', 'LIKE', '%' . Input::get('name') . '%');

		if (Input::has('adress')) // adiciona filtro por endereço
		    $query->where('adress', 'LIKE', '%' . Input::get('adress') . '%');
		
		if (Input::has('district')) // adiciona filtro por bairro
		    $query->where('district', 'LIKE', '%' . Input::get('district') . '%');
		
		if (Input::has('city')) // adiciona filtro por cidade
		   $query->where('city', 'LIKE', Input::get('city'));
		
		if (Input::has('state')) // adiciona filtro por estado
		    $query->where('state', 'LIKE', Input::get('state'));
		
		// adiciona paginação
		$places = $query->paginate(50)->appends(Input::except(array('page')));

		// Chama view
    	return View::make('place.index', compact('places'))->with('search', Input::all());
	}

	/**
	 * Exibe dados do lugar
	 *
	 * @param  int  $id - ID do lugar
	 * @return View::make
	 */
	public function show( $id )
	{
		$place = Place::find( $id );

		if ( $place ) {
			if ( Request::ajax() ) // Se for ajax
				return View::json( $place->toArray() );
			else 
				return View::make('place.show')->with('place', $place);
		} else {
			App::abort();
		}		
	}

}