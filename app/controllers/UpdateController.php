<?php

class UpdateController extends BaseController {

	/**
	 * Action inicial de controller, exibe form para atualizar sistema
	 * @return View::make
	 */
	public function index()
	{
		$page = 1;
    	return View::make('update.index', compact('page'));
	}

	/**
	 * Método que lê e faz o update dos dados
	 * @return View::make
	 */
	public function open()
	{
		// Verifica se existe um arquivo
		if (Input::hasFile('file'))
		{
			ini_set('max_execution_time', 0);
			// Pega arquivo
		  	$file   = Input::file('file');
		  	// Faz o upload
			$upload = $file->move( public_path() .'/files/', $file->getClientOriginalName());

			ini_set('auto_detect_line_endings',TRUE);
			// Lê o arquivo
			$handle = fopen( public_path() .'/files/' . $file->getClientOriginalName(),'r' );

			if ($handle) {				

				while (!feof($handle)) {
					$data = fgetcsv( $handle, 0, ';' );

					if (!empty($data[0])) {
						$place = new Place();
						$place->name  = utf8_encode($data[0]);
						$place->phone = $data[1];
						if (!empty($data[2])) {
							$endereco = explode(',', $data[2]);
						}
						$place->adress        = isset($endereco[0]) ? trim(utf8_encode($endereco[0])) : null;
						$place->adress_number = isset($endereco[1]) ? trim($endereco[1]) : null;
						$place->complement    = trim(utf8_encode($data[3]));
						$place->district      = trim(utf8_encode($data[4]));
						$place->state         = Request::input('state');
						$place->city          = Request::input('city');
						$place->save();
					}					
				}

				fclose($handle);
			}			
		} else {
			Redirect::to('update');
		}

		$page = 1;
    	return View::make('update.index', compact('page'));
	}

	/**
	 * Busca latitude e longitude de acordo com o api da google maps
	 * @return string
	 */
	public function maps()
	{	
		// Remove limite de tempo de execução
		ini_set('max_execution_time', 0);

		// Busca todos os registros que estejam com a latitude vazia
		$places = Place::whereRaw('lat IS NULL')->get();

		// Percorre registros
		foreach ($places as $place) {
			$adress = self::createGoogleMapsAdress( $place );
			$url = "http://maps.google.com/maps/api/geocode/json?address={$adress}&sensor=false";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$response = curl_exec($ch);
			curl_close($ch);
			$response_a = json_decode($response);
			$lat  = $response_a->results[0]->geometry->location->lat;
			$long = $response_a->results[0]->geometry->location->lng;

			// Atualiza
			$place->lat = $lat;
			$place->log = $long;
			$place->save();
		}

		echo "Ok";
	}

	/**
	 * Monta endereço no formato de busca do google maps
	 * @param  Place  $place Lugar
	 * @return string        Endereço formatado
	 */
	private function createGoogleMapsAdress( Place $place )
	{
		$adress   = str_replace(' ', '+', $place->adress);
		$district = str_replace(' ', '+', $place->district);
		$city     = str_replace(' ', '+', $place->city);
		$state    = str_replace(' ', '+', $place->state);
		return $adress . ',' . trim($place->adress_number) . '+' . $district . '+' . $city . '+' . $state;
	}

}