<?php
/**
 * Página inicial
 * @author Carlos Augusto Gartner <github.com/cagarter>
 */
class HomeController extends BaseController {
	
	/**
	 * Pagina inicial
	 * @return View::make
	 */
	public function index()
	{
    	return View::make('home.index', compact('places'));
	}

}