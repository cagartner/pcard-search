<?php

class ContactController extends BaseController {

	/**
	 * Tela inicia de contato
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('contact.index', compact('places'));
	}

	/**
	 * Salva e envia mensagem para contato@carlosgartner.com.br
	 *
	 * @return Response
	 */
	public function send()
	{
		// Cria regras de validação
		$rules = array(
			'name'    => 'required|min:3',
			'email'   => 'required|min:3|email',
			'message' => 'required|min:3'
        );

		// Executa validação
		$validator = Validator::make(Input::all(), $rules);

		// Se não passou na validação
		if ($validator->fails())
		  	return Redirect::to('/contact')->withInput()->withErrors($validator);

		// Cria contato
		$contact          = new Contact();
		$contact->name    = Input::get('name');
		$contact->email   = Input::get('email');
		$contact->message = Input::get('message');
		$contact->send 	  = 1;

		// Envia e-mail
		Mail::send('emails.contact', array('contact' => $contact), function($message) use ($contact)
		{
			$message->from($contact->email, $contact->name)
		    		->to('contato@carlosgartner.com.br', 'Carlos Augusto Gartner')
		    		->subject('Novo contato feito pelo site Personal - Busca!');
		});

		// Salva
		$contact->save();

		// Exibe mensagem de sucesso
		return Redirect::to('/contact')->with('success', 'Mensagem enviada com sucesso, obrigado pelo seu feedback!');
	}

}